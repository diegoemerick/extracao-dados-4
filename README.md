# Extração dados README v1.0.0

# Objetivo deste trabalho

O script irá percorrer a página informada (default: 'http://g1.globo.com/economia/mercados/cotacoes/moedas/'), transformar em um objeto e trabalhar seus dados exportando-o para um .csv. Destarte utilizamos a linguagem python para mostrar como é feita a leitura de dados de uma página e sua utilização em um mundo real. Há diferentes formas de se utilizar o script, uma delas é adicionar ao um 'cron' que ficaria de tempos em tempos acompanhando o resultado encontrado e compará-los em um gráfico a partir de um csv ou afins.

1) Instalação do python (Linux)

	# apt-get update
	# apt-get install python -y
	# sudo apt-get install bypthon -y (opcional)
	
	Obs.: Caso todas as bibliotecas não tenham sido instaladas utilize o comando: sudo apt-get -f install -y
	
2) Instalação do urllib2

	
Documentação: https://docs.python.org/2/library/urllib2.html

3) Instalação do Beautiful Soup
	
	# apt-get install python-pip -y
	# pip install beautifulsoup4

Sobre o pip: http://www.devfuria.com.br/linux/instalando-pip/
Documentação: https://www.crummy.com/software/BeautifulSoup/

4) Execução do script

	$ python analise-dados.py	
	
# Notas
Recomendamos o desenvolvedor realizar um fork do projeto e acrescentar suas ideias ao script
